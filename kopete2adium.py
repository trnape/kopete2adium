#!/usr/bin/env python3

# Kopete to Adium chat history converter
# Copyright (C) 2014 Petr Trnak
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from xml.dom import minidom
import os
import argparse
import html.parser


def main():
    parser = argparse.ArgumentParser(description ='Kopete to Adium chat history converter creates Adium chatlog based on Kopete logs.')
    parser.add_argument('--version', action='version', version='Kopete to Adium chat history converter 0.2')
    parser.add_argument('--src', required=True, type=str, help='Input directory that contains Kopete xml files')
    parser.add_argument('--dst', required=True, type=str, help='Output directory that is about to be filled with directories (one directory per contact name)')
    parser.add_argument('--acc', required=True, type=str, help='Account name, eg. john@jabber.org, 123456789, jo.hny@gmail.com')
    parser.add_argument('--svc', required=True, type=str, help='Service type, eg. Jabber, GTalk, ICQ')
   
    args = parser.parse_args()
    src_dir = args.src.rstrip('/') + '/'
    dst_dir = args.dst.rstrip('/') + '/'
    
    htmlparser = html.parser.HTMLParser()
    
    for filename in os.listdir(src_dir):
        filepath = src_dir + filename
        try:
          xmldoc = minidom.parse(filepath)
        except Exception as e:
          print('Failed to parse: {}\n{}'.format(filepath, e))
          break
          
        contact = filename.split('.', 1)[0].replace('-', '.')
        date = xmldoc.getElementsByTagName('date').item(0)
        date_year = int(date.attributes['year'].value, 10)
        date_month = int(date.attributes['month'].value, 10)
        
        output = u'<?xml version="1.0" encoding="UTF-8" ?>\n'
        output += '<chat xmlns="{}" account="{}" service="{}" adiumversion="{}" buildid="{}">\n'.format(
          "http://purl.org/net/ulf/ns/0.4-02", args.acc, args.svc, "1.5.9", "213f4163a8b5"
        )
        
        for msg in xmldoc.getElementsByTagName('msg'):
          sender = msg.attributes['from'].value
          time = msg.attributes['time'].value
          date_day, time = time.split(' ', 1)
          date_day = int(date_day, 10)
          time_h, time_m, time_s = time.split(':')
          time_h = int(time_h, 10)
          time_m = int(time_m, 10)
          time_s = int(time_s, 10)
          time = '{:0>4}-{:0>2}-{:0>2}T{:0>2}:{:0>2}:{:0>2}+0100'.format(
            date_year, date_month, date_day, time_h, time_m, time_s
          )
          textElement = msg.firstChild
          if not textElement:
            print("Empty message: " + msg.toxml())
            continue
          text = '<br />'.join(htmlparser.unescape(textElement.toxml()).split('\n'))
          output += '<message sender="{}" time="{}">'.format(sender, time)
          output += text
          output += '</message>\n';
          
        output += u'</chat>'
        
        contact_dir = dst_dir + contact
        contact_timestamp = '{} ({:0>4}-{:0>2}-01T00.00.00+0100)'.format(
          contact, date_year, date_month
        )
        chatlog_dir = '{}/{}.chatlog'.format(
          contact_dir, contact_timestamp
        )
        
        chatlog_path = '{}/{}.xml'.format(
          chatlog_dir, contact_timestamp
        )
        
        os.makedirs(chatlog_dir, exist_ok=True)
               
        with open(chatlog_path, 'w') as file:
            file.write(output)
            




if __name__ == "__main__":
    main()
