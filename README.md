# Kopete to Adium chat history converter

## Usage
To convert Google Talk (Jabber, ICQ) account chat history from Kopete format to Adium format:

    ./kopete2adium.py --src ~/.kde4/share/apps/kopete/logs/JabberProtocol/user-name\@gmail-com/  --dst /tmp/AdiumLogs/GTalk.user.name\@gmail.com/ --svc GTalk --acc "user.name@gmail.com"

    ./kopete2adium.py --src ~/.kde4/share/apps/kopete/logs/JabberProtocol/john\@jabber-net/  --dst /tmp/AdiumLogs/Jabber.john\@jabber.net/ --svc Jabber --acc "john@jabber.net"

    ./kopete2adium.py --src ~/.kde4/share/apps/kopete/logs/ICQProtocol/123456789/  --dst /tmp/AdiumLogs/ICQ.12345679/ --svc ICQ --acc "123456789"

